//
//  Item.swift
//  ToDoList
//
//  Created by Maciek Janik on 25/12/2023.
//

import Foundation
import SwiftData

@Model
final class Item {
    var timestamp: Date
    var name: String
    var isCompleted: Bool
    
    
    init(timestamp: Date, name: String) {
        self.timestamp = timestamp
        self.name = name
        self.isCompleted = false
    }
}
